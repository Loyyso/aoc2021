#include <fstream>
#include <bitset>
#include <cmath>
#include "gtest/gtest.h"

#define NB_SIZE 12

TEST(Day3, Part1) {
  std::ifstream file("resources/day3_input.txt");
  std::vector<std::bitset<NB_SIZE>> input;

  std::string line;
  while (std::getline(file, line)) {
    input.emplace_back(line);
  }
  file.close();

  std::bitset<NB_SIZE> gamma_rate;
  std::bitset<NB_SIZE> epsilon_rate;
  for (int i=0; i<NB_SIZE; i++) {
    int total = 0;
    for (const auto bits : input) {
      total += bits[i];
    }
    gamma_rate[i] = total > input.size()/2;
    epsilon_rate[i] = !gamma_rate[i];
  }
  std::cout << gamma_rate.to_ulong() * epsilon_rate.to_ulong() << std::endl;
}

std::bitset<NB_SIZE> find_rating(std::vector<std::bitset<NB_SIZE>> input, bool most_common) {
  for (int i=0; i<NB_SIZE; i++) {
    if (input.size() == 1) break;

    int total = 0;
    for (const auto bits : input) {
      total += bits[NB_SIZE-1-i];
    }

    std::vector<std::bitset<NB_SIZE>> to_remove;
    for (auto bits : input) {
      if ((most_common && bits[NB_SIZE-1-i] != total >= std::ceil(input.size()/2.0))
          || (!most_common && bits[NB_SIZE-1-i] == total >= std::ceil(input.size()/2.0))) {
        to_remove.push_back(bits);
      }
    }
    for (auto bits : to_remove) {
      input.erase(std::find(input.begin(), input.end(), bits));
    }
  }
  return input.at(0);
}

TEST(Day3, Part2) {
  std::ifstream file("resources/day3_input.txt");
  std::vector<std::bitset<NB_SIZE>> input;

  std::string line;
  while (std::getline(file, line)) {
    input.emplace_back(line);
  }
  file.close();

  std::bitset<NB_SIZE> oxygen_generator_rating = find_rating(input, true);
  std::bitset<NB_SIZE> CO2_scrubber_rating = find_rating(input, false);

  std::cout << oxygen_generator_rating.to_ulong() * CO2_scrubber_rating.to_ulong() << std::endl;
}
