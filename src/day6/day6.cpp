#include <fstream>
#include <deque>
#include "gtest/gtest.h"

#define DAYS_PART1 80
#define DAYS_PART2 256

TEST(Day6, Part1) {
  std::ifstream file("resources/day6_input.txt");
  std::vector<int> fish;

  std::string line;
  std::getline(file, line);
  file.close();
  std::stringstream nb_stream(line);

  std::string nb;
  while(std::getline(nb_stream, nb, ',')) {
    fish.push_back(std::stoi(nb));
  }

  for (int i=0; i<DAYS_PART1; i++) {
    int size = fish.size();
    for (int current_fish=0; current_fish<size; current_fish++) {
      if (fish[current_fish] == 0) {
        fish[current_fish] = 6;
        fish.push_back(8);
      } else {
        fish[current_fish]--;
      }
    }
  }
  std::cout << fish.size() << std::endl;
}


TEST(Day6, Part2) {
  std::ifstream file("resources/day6_input.txt");
  std::deque<long long> fish;
  for (long long i=0; i<=8; i++) {
    fish.push_back(0);
  }

  std::string line;
  std::getline(file, line);
  file.close();
  std::stringstream nb_stream(line);

  std::string nb;
  while(std::getline(nb_stream, nb, ',')) {
    fish[std::stol(nb)]++;
  }

  for (int i=0; i<DAYS_PART2; i++) {
    fish.push_back(fish.front());
    fish.pop_front();
    fish[6]+= fish.back();
  }

  long long total = 0;
  for (auto val : fish) {
    total += val;
  }

  std::cout << total << std::endl;
}
