#include <fstream>
#include "gtest/gtest.h"

enum Direction {
  UP,
  DOWN,
  FORWARD
};

TEST(Day2, Part1) {
  std::ifstream file("resources/day2_input.txt");
  std::vector<std::pair<Direction, int>> commands;

  std::string line;
  while (std::getline(file, line)) {
    int split = line.find(' ');

    Direction cmd;
    int val = std::stoi(line.substr(split, line.length()-1));

    std::string cmd_str = line.substr(0, split);
    if (cmd_str == "up") cmd = UP;
    else if (cmd_str == "down") cmd = DOWN;
    else cmd = FORWARD;

    commands.emplace_back(cmd, val);
  }
  file.close();

  int position = 0;
  int depth = 0;

  for (const auto entry : commands) {
    switch (entry.first) {
      case DOWN:
        depth += entry.second;
        break;
      case UP:
        depth -= entry.second;
        break;
      case FORWARD:
        position += entry.second;
        break;
    }
  }
  std::cout << position * depth << std::endl;
}

TEST(Day2, Part2) {
  std::ifstream file("resources/day2_input.txt");
  std::vector<std::pair<Direction, int>> commands;

  std::string line;
  while (std::getline(file, line)) {
    int split = line.find(' ');

    Direction cmd;
    int val = std::stoi(line.substr(split, line.length()-1));

    std::string cmd_str = line.substr(0, split);
    if (cmd_str == "up") cmd = UP;
    else if (cmd_str == "down") cmd = DOWN;
    else cmd = FORWARD;

    commands.emplace_back(cmd, val);
  }
  file.close();

  int position = 0;
  int depth = 0;
  int aim = 0;

  for (const auto entry : commands) {
    switch (entry.first) {
      case DOWN:
        aim += entry.second;
        break;
      case UP:
        aim -= entry.second;
        break;
      case FORWARD:
        position += entry.second;
        depth += aim * entry.second;
        break;
    }
  }
  std::cout << position * depth << std::endl;
}
