#include <fstream>
#include "gtest/gtest.h"
#include "day4.h"

void Bingo::addRow(const std::vector<int>& row) {
  grid.push_back(row);
}

bool Bingo::mark(int nb) {
  for (auto &row : grid) {
    for (int i=0; i<row.size(); i++) {
      if (row[i] == nb) {
        row[i] = -1;
        return won = is_winner();
      }
    }
  }
  return false;
}

bool Bingo::is_winner() {
  for (auto row : grid) {
    bool winner_row = true;
    for (int &i : row) {
      if (i != -1) {
        winner_row = false;
        break;
      }
    }
    if (winner_row)
      return true;
  }

  for (int col=0; col<grid.size(); col++) {
    bool winner_col = true;
    for (auto row : grid) {
      if (row[col] != -1) {
        winner_col = false;
      }
    }
    if (winner_col)
      return true;
  }
  return false;
}

int Bingo::get_sum() {
  int total = 0;
  for (auto row : grid) {
    for (auto val : row) {
      if (val != -1) {
        total += val;
      }
    }
  }
  return total;
}


TEST(Day4, Part1) {
  std::vector<std::shared_ptr<Bingo>> bingos;
  std::ifstream file("resources/day4_input.txt");

  std::string drawn;
  std::getline(file, drawn);

  std::string line;
  std::shared_ptr<Bingo> bingo;
  while (std::getline(file, line)) {
    if (line.empty() || line == "\r") {
      bingo = std::make_shared<Bingo>();
      bingos.push_back(bingo);
    } else {
      std::vector<int> row;
      std::stringstream stream(line);
      std::string nb;
      while (std::getline(stream, nb, ' ')) {
        if (!nb.empty() && nb != "\r") {
          row.push_back(std::stoi(nb));
        }
      }
      bingo->addRow(row);
    }
  }
  file.close();

  std::vector<int> drawn_nbs;
  std::string nb;
  std::stringstream drawnstream(drawn);
  while (getline(drawnstream, nb, ',')) {
    if (!nb.empty()) {
      drawn_nbs.push_back(std::stoi(nb));
    }
  }

  bool end = false;
  int sum = 0;
  for (auto n : drawn_nbs) {
    if (end) break;
    for (auto b : bingos) {
      if (b->mark(n)) {
        sum = n * b->get_sum();
        end = true;
        break;
      }
    }
  }

  std::cout << sum << std::endl;
}

TEST(Day4, Part2) {
  std::vector<std::shared_ptr<Bingo>> bingos;
  std::ifstream file("resources/day4_input.txt");

  std::string drawn;
  std::getline(file, drawn);

  std::string line;
  std::shared_ptr<Bingo> bingo;
  while (std::getline(file, line)) {
    if (line.empty() || line == "\r") {
      bingo = std::make_shared<Bingo>();
      bingos.push_back(bingo);
    } else {
      std::vector<int> row;
      std::stringstream stream(line);
      std::string nb;
      while (std::getline(stream, nb, ' ')) {
        if (!nb.empty() && nb != "\r") {
          row.push_back(std::stoi(nb));
        }
      }
      bingo->addRow(row);
    }
  }
  file.close();

  std::vector<int> drawn_nbs;
  std::string nb;
  std::stringstream drawnstream(drawn);
  while (getline(drawnstream, nb, ',')) {
    if (!nb.empty()) {
      drawn_nbs.push_back(std::stoi(nb));
    }
  }

  int sum = 0;
  for (auto n : drawn_nbs) {
    for (auto b : bingos) {
      if (b->won) continue;
      if (b->mark(n)) {
        sum = n * b->get_sum();
      }
    }
  }

  std::cout << sum << std::endl;
}
