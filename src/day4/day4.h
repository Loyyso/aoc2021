#ifndef AOC_DAY4_H
#define AOC_DAY4_H

class Bingo {
public:
  void addRow(const std::vector<int>&);
  bool mark(int nb);
  int get_sum();
  bool won;
private:
  std::vector<std::vector<int>> grid;
  bool is_winner();
};

#endif
