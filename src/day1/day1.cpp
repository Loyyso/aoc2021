#include <fstream>
#include "gtest/gtest.h"

TEST(Day1, Part1) {
  std::vector<int> input;
  std::ifstream file("resources/day1_input.txt");

  std::string line;
  while (std::getline(file, line)) {
    input.push_back(std::stoi(line));
  }
  file.close();

  int larger_measurements = 0;
  for (int i=1; i<input.size(); i++) {
    if (input.at(i) > input.at(i-1))
      larger_measurements++;
  }

  std::cout << larger_measurements << std::endl;
}

TEST(Day1, Part2) {
  std::vector<int> input;
  std::ifstream file("resources/day1_input.txt");

  std::string line;
  while (std::getline(file, line)) {
    input.push_back(std::stoi(line));
  }
  file.close();

  int larger_measurements = 0;
  for (int i=3; i<input.size(); i++) {
    int sliding_window_A = input.at(i-3) + input.at(i-2) + input.at(i-1);
    int sliding_window_B = input.at(i-2) + input.at(i-1) + input.at(i);
    if (sliding_window_B > sliding_window_A)
      larger_measurements++;
  }

  std::cout << larger_measurements << std::endl;
}
