#include <fstream>
#include <deque>
#include <algorithm>

#include "gtest/gtest.h"

TEST(Day10, Part1) {
  std::ifstream file("resources/day10_input.txt");
  std::vector<std::string> lines;

  {
    std::string line;
    while (std::getline(file, line)) {
      if (!line.empty()) lines.push_back(line);
    }
    file.close();
  }

  int score = 0;
  for (const auto& line : lines) {
    bool stop = false;
    std::deque<char> lifo;
    for (const auto& c : line) {
      switch (c) {
        case '(':
        case '[':
        case '{':
        case '<':
          lifo.push_front(c);
          break;
        case ')':
          if (lifo.front() != '(') {
            score += 3;
            stop = true;
          } else {
            lifo.pop_front();
          }
          break;
        case ']':
          if (lifo.front() != '[') {
            score += 57;
            stop = true;
          } else {
            lifo.pop_front();
          }
          break;
        case '}':
          if (lifo.front() != '{') {
            score += 1197;
            stop = true;
          } else {
            lifo.pop_front();
          }
          break;
        case '>':
          if (lifo.front() != '<') {
            score += 25137;
            stop = true;
          } else {
            lifo.pop_front();
          }
          break;
        default:
          break;
      }
      if (stop) break;
    }
  }

  std::cout << score << std::endl;
}

TEST(Day10, Part2) {
  std::ifstream file("resources/day10_input.txt");
  std::vector<std::string> lines;

  {
    std::string line;
    while (std::getline(file, line)) {
      if (!line.empty()) lines.push_back(line);
    }
    file.close();
  }

  std::vector<long long> scores;
  for (auto& line : lines) {
    bool corrupted = false;
    std::deque<char> lifo;
    for (const auto& c : line) {
      switch (c) {
        case '(':
        case '[':
        case '{':
        case '<':
          lifo.push_front(c);
          break;
        case ')':
          if (lifo.front() != '(') corrupted = true;
          else lifo.pop_front();
          break;
        case ']':
          if (lifo.front() != '[') corrupted = true;
          else lifo.pop_front();
          break;
        case '}':
          if (lifo.front() != '{') corrupted = true;
          else lifo.pop_front();
          break;
        case '>':
          if (lifo.front() != '<') corrupted = true;
          else lifo.pop_front();
          break;
        default:
          break;
      }
      if (corrupted) break;
    }
    if (corrupted) continue;
    long long score = 0;
    while (!lifo.empty()) {
      score*=5;
      switch (lifo.front()) {
        case '(':
          line.push_back(')');
          score+=1;
          break;
        case '[':
          line.push_back(']');
          score+=2;
          break;
        case '{':
          line.push_back('}');
          score+=3;
          break;
        case '<':
          line.push_back('>');
          score+=4;
          break;
        default:
          break;
      }
      lifo.pop_front();
    }
    scores.push_back(score);
  }

  std::sort(scores.begin(),  scores.end());
  std::cout << scores.at(scores.size()/2) << std::endl;
}
