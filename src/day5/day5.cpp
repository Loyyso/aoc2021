#include <fstream>
#include "gtest/gtest.h"
#include "day5.h"

#define GRID_SIZE 1000

Grid::Grid() {
  for (int i=0; i<GRID_SIZE; i++) {
    std::vector<int> row;
    for (int j=0; j<GRID_SIZE; j++) {
      row.push_back(0);
    }
    grid.push_back(row);
  }
}

void Grid::addLine(int x1, int y1, int x2, int y2) {
  if (x1 == x2) {
    int min, max;
    if (y1 < y2) {
      min = y1;
      max = y2;
    } else {
      min = y2;
      max = y1;
    }
    for (int i=min; i<=max; i++) {
      grid[i][x1] += 1;
    }
  } else if (y1 == y2) {
    int min, max;
    if (x1 < x2) {
      min = x1;
      max = x2;
    } else {
      min = x2;
      max = x1;
    }
    for (int i=min; i<=max; i++) {
      grid[y1][i] += 1;
    }
  }
}

void Grid::addLineDiagonals(int x1, int y1, int x2, int y2) {
  if (x1 == x2) {
    int min, max;
    if (y1 < y2) {
      min = y1;
      max = y2;
    } else {
      min = y2;
      max = y1;
    }
    for (int i=min; i<=max; i++) {
      grid[i][x1] += 1;
    }
  } else if (y1 == y2) {
    int min, max;
    if (x1 < x2) {
      min = x1;
      max = x2;
    } else {
      min = x2;
      max = x1;
    }
    for (int i=min; i<=max; i++) {
      grid[y1][i] += 1;
    }
  } else {
    int x = x1;
    int y = y1;
    while (x != x2 && y != y2) {
      grid[y][x] += 1;
      x2 > x ? x++ : x--;
      y2 > y ? y++ : y--;
    }
    grid[y][x] += 1;
  }
}

int Grid::getOverlaps() {
  int total = 0;
  for (auto row : grid) {
    for (auto val : row) {
      if (val > 1) total++;
    }
  }
  return total;
}

TEST(Day5, Part1) {
  std::ifstream file("resources/day5_input.txt");
  Grid grid;

  std::string line;
  while (std::getline(file, line)) {
    size_t pos = line.find(" -> ");
    std::string first_pos = line.substr(0, pos);
    size_t comma = first_pos.find(',');
    int x1 = std::stoi(first_pos.substr(0, comma));
    int y1 = std::stoi(first_pos.substr(comma+1, first_pos.length()));
    std::string second_pos = line.substr(pos+4, line.length());
    comma = second_pos.find(',');
    int x2 = std::stoi(second_pos.substr(0, comma));
    int y2 = std::stoi(second_pos.substr(comma+1, first_pos.length()));
    grid.addLine(x1, y1, x2, y2);

  }
  file.close();

  std::cout << grid.getOverlaps() << std::endl;
}

TEST(Day5, Part2) {
  std::ifstream file("resources/day5_input.txt");
  Grid grid;

  std::string line;
  while (std::getline(file, line)) {
    size_t pos = line.find(" -> ");
    std::string first_pos = line.substr(0, pos);
    size_t comma = first_pos.find(',');
    int x1 = std::stoi(first_pos.substr(0, comma));
    int y1 = std::stoi(first_pos.substr(comma+1, first_pos.length()));
    std::string second_pos = line.substr(pos+4, line.length());
    comma = second_pos.find(',');
    int x2 = std::stoi(second_pos.substr(0, comma));
    int y2 = std::stoi(second_pos.substr(comma+1, first_pos.length()));
    grid.addLineDiagonals(x1, y1, x2, y2);

  }
  file.close();

  std::cout << grid.getOverlaps() << std::endl;
}
