#ifndef AOC_DAY5_H
#define AOC_DAY5_H

class Grid {
public:
  Grid();
  void addLine(int x1, int y1, int x2, int y2);
  void addLineDiagonals(int x1, int y1, int x2, int y2);
  int getOverlaps();
private:
  std::vector<std::vector<int>> grid;
};

#endif
