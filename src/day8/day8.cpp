#include <fstream>
#include <map>
#include <algorithm>

#include "gtest/gtest.h"

TEST(Day8, Part1) {
  std::ifstream file("resources/day8_input.txt");

  std::vector<std::string> lines;
  {
    std::string line;
    while (std::getline(file, line)) {
      lines.push_back(line);
    }
  }
  file.close();

  std::vector<std::string> unique_values;
  for (auto line : lines) {
    std::stringstream l(line);
    std::string second_part;
    std::getline(l, second_part, '|');
    std::getline(l, second_part, '|');

    std::stringstream second_part_stream(second_part);
    for (int i=0; i<5; i++) {
      std::string val;
      std::getline(second_part_stream, val, ' ');
      if (val.length() == 2
        || val.length() == 4
        || val.length() == 3
        || val.length() == 7) {
        unique_values.push_back(val);
      }
    }
  }

  std::cout << unique_values.size() << std::endl;
}

std::map<std::set<char>, int> decode(std::set<std::string> stringset) {
  std::map<char, char> ctoc; // char to encoded char
  std::map<int, std::set<char>> ntocs; // number to encoded char set

  // finding unique charsets for 1, 4, 7, and 8
  {
    std::string one = *std::find_if(stringset.begin(), stringset.end(), [&](const std::string& item){
      return item.length() == 2;
    });
    ntocs.emplace(1, std::set<char>(one.begin(), one.end()));

    std::string four = *std::find_if(stringset.begin(), stringset.end(), [&](const std::string& item){
      return item.length() == 4;
    });
    ntocs.emplace(4, std::set<char>(four.begin(), four.end()));

    std::string seven = *std::find_if(stringset.begin(), stringset.end(), [&](const std::string& item){
      return item.length() == 3;
    });
    ntocs.emplace(7, std::set<char>(seven.begin(), seven.end()));

    std::string eight = *std::find_if(stringset.begin(), stringset.end(), [&](const std::string& item){
      return item.length() == 7;
    });
    ntocs.emplace(8, std::set<char>(eight.begin(), eight.end()));
  }

  // finding a
  {
    std::set<char> difference;
    std::set_difference(ntocs.at(7).begin(), ntocs.at(7).end(),
                        ntocs.at(1).begin(), ntocs.at(1).end(), std::inserter(difference, difference.begin()));
    ctoc.emplace('a', *difference.begin());
  }

  // finding 9
  {
    std::set<char> four_plus_a = ntocs.at(4);
    four_plus_a.insert(ctoc.at('a'));
    for (const auto& str : stringset) {
      std::set<char> charset(str.begin(), str.end());
      if (charset.size() != 6) continue;
      std::set<char> difference;
      std::set_difference(charset.begin(),  charset.end(),
                          four_plus_a.begin(),  four_plus_a.end(), std::inserter(difference, difference.begin()));
      std::set<char> intersection;
      std::set_intersection(four_plus_a.begin(),  four_plus_a.end(),
                            charset.begin(),  charset.end(), std::inserter(intersection, intersection.begin()));
      if (difference.size() != 1 || intersection.size() != 5) continue;
      ntocs.emplace(9, charset);
    }
  }

  // finding e
  {
    std::set<char> difference;
    std::set_difference(ntocs.at(8).begin(), ntocs.at(8).end(),
                        ntocs.at(9).begin(), ntocs.at(9).end(), std::inserter(difference, difference.begin()));
    ctoc.emplace('e', *difference.begin());
  }

  // finding 2, 5 and 3
  for (const auto& str : stringset) {
    std::set<char> charset(str.begin(), str.end());
    if (charset.size() != 5) continue;
    std::set<char> intersection;
    std::set_intersection(ntocs.at(1).begin(),  ntocs.at(1).end(),
                          charset.begin(),  charset.end(), std::inserter(intersection, intersection.begin()));
    if (intersection.size() == 2) {
      ntocs.emplace(3, charset);
      continue;
    }
    if (charset.find(ctoc.at('e')) != charset.end()) {
      ntocs.emplace(2, charset);
    } else {
      ntocs.emplace(5, charset);
    }
  }

  // finding 0 and 6
  for (const auto& str : stringset) {
    for (int i=0; i<2; i++) {
      if (str.length() != 6) continue;
      std::set<char> charset(str.begin(), str.end());

      bool found = false;
      for (const auto &entry: ntocs) {
        if (charset == entry.second) found = true;
      }
      if (found) break;

      std::set<char> intersection;
      std::set_intersection(charset.begin(), charset.end(), ntocs.at(1).begin(),
                            ntocs.at(1).end(), std::inserter(intersection, intersection.begin()));
      if (intersection.size() == 1) {
        ntocs.emplace(6, charset);
      } else {
        ntocs.emplace(0, charset);
      }
    }
  }

  // reverse the map
  std::map<std::set<char>, int> res;
  for (const auto entry : ntocs) {
    res.emplace(entry.second, entry.first);
  }
  return res;
}

TEST(Day8, Part2) {
  std::ifstream file("resources/day8_input.txt");

  std::vector<std::pair<std::set<std::string>, std::vector<std::set<char>>>> lines;

  {
    std::string line;
    while (std::getline(file, line)) {
      if (line.at(line.length()-1) == '\r')
        line.pop_back();

      std::string first_part;
      std::string second_part;
      std::stringstream line_stream(line);
      std::getline(line_stream, first_part, '|');
      std::getline(line_stream, second_part, '|');

      std::stringstream first_part_stream(first_part);
      std::stringstream second_part_stream(second_part);
      std::string val;

      std::set<std::string> first;
      std::vector<std::set<char>> second;
      while(std::getline(first_part_stream, val, ' ')) {
        first.insert(val);
      }
      while(std::getline(second_part_stream, val, ' ')) {
        if (val.empty()) continue;
        second.emplace_back(val.begin(),  val.end());
      }
      lines.emplace_back(first, second);
    }
  }
  file.close();

  int total = 0;
  for (const auto line : lines) {
    std::map<std::set<char>, int> res = decode(line.first);
    std::string res_nb;
    for (const auto& nb : line.second) {
      res_nb += std::to_string(res.at(nb));
    }
    total += std::stoi(res_nb);
  }

  std::cout << total << std::endl;
}
