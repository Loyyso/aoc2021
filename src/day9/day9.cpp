#include <fstream>
#include <deque>
#include <algorithm>

#include "gtest/gtest.h"

TEST(Day9, Part1) {
  std::ifstream file("resources/day9_input.txt");
  std::vector<std::vector<int>> heightmap;

  {
    std::string line;
    while (std::getline(file, line)) {
      std::vector<int> heights;
      for (auto c : line) {
        if (c == '\r') continue;
        heights.push_back(std::stoi(std::string(1, c)));
      }
      heightmap.push_back(heights);
    }
    file.close();
  }

  int sum = 0;
  for (int y=0; y<heightmap.size(); y++) {
    for (int x=0; x<heightmap.at(y).size(); x++) {
      int val = heightmap.at(y).at(x);
      if (x > 0 && heightmap.at(y).at(x - 1) <= val) continue;
      if (x < heightmap.at(y).size()-1 && heightmap.at(y).at(x + 1) <= val) continue;
      if (y > 0 && heightmap.at(y - 1).at(x) <= val) continue;
      if (y < heightmap.size()-1 && heightmap.at(y + 1).at(x) <= val) continue;
      sum += val+1;
    }
  }
  std::cout << sum << std::endl;
}

TEST(Day9, Part2) {
  std::ifstream file("resources/day9_input.txt");
  std::vector<std::vector<int>> heightmap;

  {
    std::string line;
    while (std::getline(file, line)) {
      std::vector<int> heights;
      for (auto c : line) {
        if (c == '\r') continue;
        heights.push_back(std::stoi(std::string(1, c)));
      }
      heightmap.push_back(heights);
    }
    file.close();
  }

  std::vector<std::set<std::pair<int, int>>> basins;
  std::set<std::pair<int, int>> visited;
  for (int y=0; y<heightmap.size(); y++) {
    for (int x=0; x<heightmap.at(y).size(); x++) {
      if (visited.find(std::make_pair(x, y)) != visited.end()) continue;
      visited.emplace(x, y);
      int val = heightmap.at(y).at(x);
      if (val == 9) continue;
      std::set<std::pair<int, int>> basin;
      basin.emplace(x, y);

      std::deque<std::pair<int, int>> fifo;
      fifo.emplace_back(x, y);
      while (!fifo.empty()) {
        std::pair<int, int> coords = fifo.front();
        fifo.pop_front();
        visited.insert(coords);
        if (heightmap.at(coords.second).at(coords.first) < 9) basin.insert(coords);
        if (coords.first > 0 &&
            heightmap.at(coords.second).at(coords.first - 1) != 9
            && visited.find(std::make_pair(coords.first - 1, coords.second)) == visited.end())
          fifo.emplace_back(coords.first - 1, coords.second);
        if (coords.first < heightmap.at(coords.second).size() - 1
            && heightmap.at(coords.second).at(coords.first + 1) != 9
            && visited.find(std::make_pair(coords.first + 1, coords.second)) == visited.end())
          fifo.emplace_back(coords.first + 1, coords.second);
        if (coords.second > 0
            && heightmap.at(coords.second - 1).at(coords.first) != 9
            && visited.find(std::make_pair(coords.first, coords.second - 1)) == visited.end())
          fifo.emplace_back(coords.first, coords.second - 1);
        if (coords.second < heightmap.size() - 1
            && heightmap.at(coords.second + 1).at(coords.first) != 9
            && visited.find(std::make_pair(coords.first, coords.second + 1)) == visited.end())
          fifo.emplace_back(coords.first, coords.second + 1);
      }
      basins.push_back(basin);
    }
  }

  std::sort(basins.begin(),  basins.end(), [&](const auto& a, const auto& b) {return a.size() > b.size();});
  std::cout << basins[0].size()*basins[1].size()*basins[2].size() << std::endl;
}
