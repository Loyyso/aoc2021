#include <fstream>
#include <map>
#include <cmath>

#include "gtest/gtest.h"

TEST(Day7, Part1) {
  std::ifstream file("resources/day7_input.txt");
  std::vector<int> positions;

  std::string line;
  std::getline(file, line);
  file.close();
  std::stringstream nb_stream(line);

  std::string nb;
  while(std::getline(nb_stream, nb, ',')) {
    positions.push_back(std::stoi(nb));
  }
  int min = positions[0];
  int max = positions[0];

  for (const auto position : positions) {
    if (position > max) max = position;
    if (position < min) min = position;
  }

  std::map<int, int> fuel_cost;
  int min_fuel = INT32_MAX;

  for (int i=min; i<=max; i++) {
    fuel_cost.emplace(i, 0);
    for (const auto position : positions) {
      fuel_cost[i]+= abs(i-position);
    }
    if (fuel_cost[i] < min_fuel) min_fuel = fuel_cost[i];
  }

  std::cout << min_fuel << std::endl;
}

TEST(Day7, Part2) {
  std::ifstream file("resources/day7_input.txt");
  std::vector<int> positions;

  std::string line;
  std::getline(file, line);
  file.close();
  std::stringstream nb_stream(line);

  std::string nb;
  while(std::getline(nb_stream, nb, ',')) {
    positions.push_back(std::stoi(nb));
  }
  int min = positions[0];
  int max = positions[0];

  for (const auto position : positions) {
    if (position > max) max = position;
    if (position < min) min = position;
  }

  std::map<int, int> fuel_cost;
  int min_fuel = INT32_MAX;

  for (int i=min; i<=max; i++) {
    fuel_cost.emplace(i, 0);
    for (const auto position : positions) {
      int base_cost = abs(i-position);
      fuel_cost[i]+= base_cost * (base_cost + 1) / 2;
    }
    if (fuel_cost[i] < min_fuel) min_fuel = fuel_cost[i];
  }

  std::cout << min_fuel << std::endl;
}
