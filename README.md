# Advent of Code 2021 solutions

## Building

Clone the repository: `git clone --recurse-submodules git@gitlab.com:Loyyso/aoc2021.git && cd aoc2021`

Create a build directory: `mkdir build && cd build`

Generate build files: `cmake ..`

Compile: `make`

Run: `./aoc`
